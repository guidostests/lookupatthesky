package com.guidovezzoni.lookupatthesky.utils;

import android.app.Activity;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AlertDialog;
import android.text.TextUtils;

/*
 * Created by Guido on 17/12/2015.


 Usage sample:

 When setting up the activity:
   mPermissionRequestUtil = new PermissionRequestUtil(this, "The app needs this permission because blah bla blah...", "OK", "Cancel", new PermissionRequestUtil.OnPermissionRequest() {
       @Override
           public void onPermissionGranted() {
           // action
       }
      @Override public void onPermissionDenied() {
           // notification or other action
            Toast.makeText(MainActivity.this, "Permission Denied", Toast.LENGTH_SHORT).show();
       }
   });
   mPermissionRequestUtil.requestPermissionAndRun(Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_CODE_ASK_PERMISSIONS);


Then override activity's onRequestPermissionsResult:

    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (mPermissionRequestUtil!=null) {
                mPermissionRequestUtil.onRequestPermissionsResult(grantResults[0]);
            }
            break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

 */
public class PermissionRequestUtil {
    private static final String DEFAULT_OK_MSG = "OK";
    private static final String DEFAULT_CANCEL_MSG = "Cancel";


    private Activity mActivity;
    private OnPermissionRequest mListener;

    private String mOkMsg = DEFAULT_OK_MSG;
    private String mCancelMsg = DEFAULT_CANCEL_MSG;
    private String mDialogMsg;

    /*
    About the rationale, according to the documentation:
         * Gets whether you should show UI with rationale for requesting a permission.
         * You should do this only if you do not have the permission and the context in
         * which the permission is requested does not clearly communicate to the user
         * what would be the benefit from granting this permission.
         * <p>
         * For example, if you write a camera app, requesting the camera permission
         * would be expected by the user and no rationale for why it is requested is
         * needed. If however, the app needs location for tagging photos then a non-tech
         * savvy user may wonder how location is related to taking photos. In this case
         * you may choose to show UI with rationale of requesting this permission.
    */
    public PermissionRequestUtil(Activity activity, String rationaleMessage, String rationaleDialogOkMessage,
                                 String rationaleDialogCancelMessage, OnPermissionRequest listener) {
        this.mActivity = activity;
        this.mListener = listener;

        if (!TextUtils.isEmpty(rationaleDialogOkMessage)) {
            mOkMsg = rationaleDialogOkMessage;
        }
        if (!TextUtils.isEmpty(rationaleDialogCancelMessage)) {
            mCancelMsg = rationaleDialogCancelMessage;
        }
        if (!TextUtils.isEmpty(rationaleMessage)) {
            mDialogMsg = rationaleMessage;
        }
    }

    /*
    Constructor with rationale message, OK and cancel button are set to OK and Cancel by default
     */
    public PermissionRequestUtil(Activity activity, String rationaleMessage, OnPermissionRequest listener) {
        this(activity, rationaleMessage, null, null, listener);
    }

    /*
    Constructor without rationale message at all
     */
    public PermissionRequestUtil(Activity activity, OnPermissionRequest listener) {
        this(activity, null, null, null, listener);
    }


    public interface OnPermissionRequest {
        void onPermissionGranted();

        void onPermissionDenied();
    }

    public void requestPermissionAndRun(final String permission, final int requestCode) {
        int hasPermission = ContextCompat.checkSelfPermission(mActivity, permission);
        if (hasPermission != PackageManager.PERMISSION_GRANTED) {
            if (!TextUtils.isEmpty(mDialogMsg) && !ActivityCompat.shouldShowRequestPermissionRationale(mActivity, permission)) {
                showMessageOKCancel(mDialogMsg,
                        new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialog, int which) {
                                ActivityCompat.requestPermissions(mActivity,
                                        new String[]{permission},
                                        requestCode);
                            }
                        });
                return;
            }
            ActivityCompat.requestPermissions(mActivity,
                    new String[]{permission},
                    requestCode);
            return;
        }
        if (mListener != null) {
            mListener.onPermissionGranted();
        }
    }

    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(mActivity)
                .setMessage(message)
                .setPositiveButton(mOkMsg, okListener)
                .setNegativeButton(mCancelMsg, null)
                .create()
                .show();
    }


    public void onRequestPermissionsResult(int grantResults) {
        if (grantResults == PackageManager.PERMISSION_GRANTED) {
            // Permission Granted
            if (mListener != null) {
                mListener.onPermissionGranted();
            }
        } else {
            // Permission Denied
            if (mListener != null) {
                mListener.onPermissionDenied();
            }
        }
    }

    public void updateMessages(String okMsg, String cancelMsg, String dialogMsg) {
        if (!TextUtils.isEmpty(okMsg)) {
            mOkMsg = okMsg;
        }
        if (!TextUtils.isEmpty(cancelMsg)) {
            mCancelMsg = cancelMsg;
        }
        if (!TextUtils.isEmpty(dialogMsg)) {
            mDialogMsg = dialogMsg;
        }
    }

    public void updateMessages(int okMsg, int cancelMsg, int dialogMsg) {
        updateMessages(mActivity.getString(okMsg), mActivity.getString(cancelMsg), mActivity.getString(dialogMsg));
    }

}
