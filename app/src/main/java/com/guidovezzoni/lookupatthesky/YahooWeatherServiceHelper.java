package com.guidovezzoni.lookupatthesky;

import android.app.Activity;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Guido on 11/06/2015.
 *
 * Handles callbacks from retrofit and some basic check on the returned data
 */
class YahooWeatherServiceHelper implements Callback<YahooWeatherAPIClient.Root> {
    IYahooServiceCaller mCaller;
    Activity mActivity;

    public YahooWeatherServiceHelper(IYahooServiceCaller mCaller, Activity mActivity) {
        this.mCaller = mCaller;
        this.mActivity = mActivity;
    }

    @Override
    public void success(YahooWeatherAPIClient.Root root, Response response) {
        // checks that all classes are instantiated before accessing the data
        if (root != null && root.query!= null && root.query.results!= null && root.query.results.channel!= null
                && root.query.results.channel.item!= null && root.query.results.channel.item.condition!= null) {
            mCaller.onYahooServiceSuccess(root.query.results.channel.item.condition);
        } else {
            mCaller.onYahooServiceError(mActivity.getString(R.string.yahoo_error_no_condition_available));
        }
    }

    @Override
    public void failure(RetrofitError error) {
        mCaller.onRetrofitError(error);
    }

    public interface IYahooServiceCaller {

        public void onYahooServiceSuccess(YahooWeatherAPIClient.Condition condition);

        public void onYahooServiceError(String error);

        public void onRetrofitError(RetrofitError error);
    }

}
