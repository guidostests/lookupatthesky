package com.guidovezzoni.lookupatthesky;

import android.Manifest;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.location.Criteria;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.ActionBarActivity;
import android.os.Bundle;
import android.support.v7.app.AlertDialog;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.Toast;

import com.google.android.gms.common.ConnectionResult;
import com.google.android.gms.common.GooglePlayServicesUtil;
import com.google.android.gms.maps.CameraUpdate;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.MapFragment;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.model.CameraPosition;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.guidovezzoni.lookupatthesky.utils.PermissionRequestUtil;

import retrofit.RetrofitError;


public class MainActivity extends ActionBarActivity implements OnMapReadyCallback,
        GoogleMap.OnCameraChangeListener, LocationListener, FlickrServiceHelper.IFlickrServiceCaller, YahooWeatherServiceHelper.IYahooServiceCaller {
    final private int REQUEST_CODE_ASK_PERMISSIONS = 123;

    private GoogleMap gMap = null;

    private Marker currentMarker = null;

    // set to true when we have a configuration changed
    private boolean configChanged;

    private View mMainActivityLayout = null;

    // used to convert the temperature in colour
    private Bitmap mBmp = null;

    private static double GREENWICH_LAT = 51.4778;
    private static double GREENWICH_LON = 0.0015;
    private static float DEFAULT_ZOOM = 8;

    private static int COLOUR_LOWER_BOUND=20;
    private static int COLOUR_HIGHER_BOUND=100;

    private PermissionRequestUtil mPermissionRequestUtil;


// *********************************************************************************************
// Lifecycle overrides - fragments/activities

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        checkPlayServicesAvailable();
        setContentView(R.layout.activity_main);

        mMainActivityLayout = findViewById(R.id.main_activity_layout);

        mBmp = BitmapFactory.decodeResource(getResources(), R.drawable.temperature);

        MapFragment mapFrag = (MapFragment) getFragmentManager().findFragmentById(R.id.map);

        configChanged = (savedInstanceState != null);

        mapFrag.getMapAsync(this);
    }

// *********************************************************************************************
// Other overrides

    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.menu_main, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();

        //noinspection SimplifiableIfStatement
        if (id == R.id.action_settings) {
            return true;
        }

        return super.onOptionsItemSelected(item);
    }

// *********************************************************************************************
// Additional protected methods

    protected boolean checkPlayServicesAvailable() {
        // simply call the appropriate API
        int status = GooglePlayServicesUtil.isGooglePlayServicesAvailable(this);

        if (status == ConnectionResult.SUCCESS) {
            return (true);
        } else {
            Toast.makeText(this, getString(R.string.no_gmaps), Toast.LENGTH_LONG).show();
            finish();
        }
        return (false);
    }

// *********************************************************************************************
// Additional private methods

    private void initMap() {
        if (!configChanged) {
            // center in Greenwich with a default zoom
            CameraUpdate center =
                    CameraUpdateFactory.newLatLng(new LatLng(GREENWICH_LAT, GREENWICH_LON));
            CameraUpdate zoom = CameraUpdateFactory.zoomTo(DEFAULT_ZOOM);
            gMap.moveCamera(center);
            gMap.animateCamera(zoom);

            mPermissionRequestUtil = new PermissionRequestUtil(this, "The app needs your location in order to find out the weather", "OK", "Cancel",
                    new PermissionRequestUtil.onPermissionRequest() {
                @Override
                public void OnPermissionGranted() {
                    // enable location manager
                    LocationManager locMgr = (LocationManager) getSystemService(LOCATION_SERVICE);
                    Criteria crit = new Criteria();
                    crit.setAccuracy(Criteria.ACCURACY_FINE);
                    locMgr.requestSingleUpdate(crit, MainActivity.this, null);
                }
                @Override
                public void OnPermissionDenied() {
                    Toast.makeText(MainActivity.this, "LOCATION_SERVICE Denied", Toast.LENGTH_SHORT).show();
                }
            });
            mPermissionRequestUtil.requestPermissionAndRun(Manifest.permission.ACCESS_FINE_LOCATION, REQUEST_CODE_ASK_PERMISSIONS);
        }
        gMap.setOnCameraChangeListener(this);
    }



    @Override
    public void onRequestPermissionsResult(int requestCode, String[] permissions, int[] grantResults) {
        switch (requestCode) {
            case REQUEST_CODE_ASK_PERMISSIONS:
                if (mPermissionRequestUtil!=null) {
                    mPermissionRequestUtil.onRequestPermissionsResult(grantResults[0]);
                }
                break;
            default:
                super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    private void updateMarkerPosition(GoogleMap map, double lat, double lon) {
        // create or update marker
        if (currentMarker == null) {
            currentMarker = map.addMarker(new MarkerOptions().position(new LatLng(lat, lon))
                    .title(getString(R.string.info_win_title))
                    .snippet(""));
        } else {
            currentMarker.setPosition(new LatLng(lat, lon));
        }
        currentMarker.hideInfoWindow();
    }

    private void updateMarkerWeather(String location, String condition) {
        if (currentMarker != null) {
            currentMarker.setTitle(location);
            currentMarker.setSnippet(condition);
            currentMarker.showInfoWindow();
        }
    }

    private void updateBackgroundColour(int fahreneit) {
        mMainActivityLayout.setBackgroundColor(getColourByRange(mBmp, fahreneit, COLOUR_LOWER_BOUND, COLOUR_HIGHER_BOUND));
    }

    // conert a position within a range into a colour taken from a bitmap
    private int getColourByRange(Bitmap bmp, int position, int lowerBound, int higherBound) {
        if (position <= lowerBound
                // error conditions are set to "colder" in order to simplify
                || lowerBound == higherBound || lowerBound > higherBound)
            return bmp.getPixel(0, 0);

        if (position >= higherBound)
            return bmp.getPixel(bmp.getWidth() - 1, 0);

        return bmp.getPixel(bmp.getWidth() * (position - lowerBound) / (higherBound - lowerBound), 0);
    }

// *********************************************************************************************
// Callback Interface Implementation

    @Override
    public void onMapReady(GoogleMap googleMap) {
        this.gMap = googleMap;
        initMap();
    }

    /*
    * on location change we update the camera
    * */
    @Override
    public void onLocationChanged(Location location) {
        LatLng latLng = new LatLng(location.getLatitude(), location.getLongitude());
        CameraUpdate cameraUpdate = CameraUpdateFactory.newLatLng(latLng);

        gMap.animateCamera(cameraUpdate);
    }

    /*
    On camera change we update marker and background color
    * */
    @Override
    public void onCameraChange(CameraPosition cameraPosition) {
//        Log.d(getClass().getSimpleName(),
//                String.format("lat: %f, lon: %f, zoom: %f, tilt: %f",
//                        cameraPosition.target.latitude, cameraPosition.target.longitude,
//                        cameraPosition.zoom, cameraPosition.tilt));

        updateMarkerPosition(gMap,
                cameraPosition.target.latitude, cameraPosition.target.longitude);

        // we use flickr to convert lat long to woeid
        // as two API need to be accessed, the activity class cannot be used as callback, two helper classes are used
        FlickrAPIClient.getService().getWoeid(FlickrAPIClient.APIKEY, Double.toString(cameraPosition.target.latitude), Double.toString(cameraPosition.target.longitude),
                new FlickrServiceHelper(this, this));
    }


    /*
    * if the call to flicker API is successful we call the yahoo API
    * */
    @Override
    public void onFlickrServiceSuccess(int woeid) {
        YahooWeatherAPIClient.getService().execYQLQuery(
                YahooWeatherAPIClient.getWeatherConditionByWoeidSQL(woeid),
                YahooWeatherAPIClient.FORMAT,
                YahooWeatherAPIClient.ENV,
                new YahooWeatherServiceHelper(this, this));
    }

    /*
    * if the yahoo API call is OK then we update the UI
    * */
    @Override
    public void onYahooServiceSuccess(YahooWeatherAPIClient.Condition condition) {
        updateMarkerWeather(getString(R.string.info_win_title), condition.text + " (temp " + Integer.toString(condition.temp) + " F)");
        updateBackgroundColour(condition.temp);
    }

    /*
    * errors on Flickr API call
    * */
    @Override
    public void onFlickrServiceError(String error) {
        Toast.makeText(this, "There was an error - ID=" + error, Toast.LENGTH_SHORT).show();
        Log.e(getClass().getSimpleName(), error, null);
    }

    /*
    * errors on Yahoo API calls
    * */
    @Override
    public void onYahooServiceError(String error) {
        Toast.makeText(this, "There was an error - ID=" + error, Toast.LENGTH_SHORT).show();
        Log.e(getClass().getSimpleName(), error, null);
    }

    /*
    * generic retrofit errors
    * */
    @Override
    public void onRetrofitError(RetrofitError error) {
        Toast.makeText(this, "There was an error - ID=" + error, Toast.LENGTH_LONG).show();
        Log.e(getClass().getSimpleName(), "Retrofit Exception", error);
    }

    // these are not used
    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
