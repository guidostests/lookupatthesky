package com.guidovezzoni.lookupatthesky;

import java.util.List;

import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Guido on 10/06/2015.
 */
public class FlickrAPIClient {
    public static final String URL = "https://api.flickr.com";

    // if this key doesn't work one can be found on https://www.flickr.com/services/api/explore/flickr.places.findByLatLon
    // seems to work for one day only as it's for testing the flickr API
    public static final String APIKEY = "dcd4e5f75b9c3a77d8516d24d5b00762";

    //json data class
    static class Root {
        Places places;
        String stat;
        // code
        String message;
    }

    //json data class
    static class Places {
        List<Place> place;
        // latitude;
        // longitude;
        // accuracy;
        // total;
    }

    //json data class
    static class Place {
        // place_id
        int woeid;
        // latitude;
        // longitude;
        // ...
    }


    // Handles the retrofix call
    interface FlickrServiceInterface {

        // in order to simplify I only consider the parameter I need for this call
        @GET("/services/rest/?method=flickr.places.findByLatLon&format=json&nojsoncallback=1")
        void getWoeid(@Query("api_key") String apiKey,
                @Query("lat") String lat,
                @Query("lon") String lon,
                        Callback<Root> callback);
    }

    // Returns a service instance, that will query the API when needed
    public static FlickrServiceInterface getService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(URL)
                .build();

        return restAdapter.create(FlickrServiceInterface.class);
    }

}


    /* example:

    https://api.flickr.com/services/rest/?method=flickr.places.findByLatLon&api_key=f668ba7021ea19baae5f964dde71b578&lat=51.4&lon=0&format=json&nojsoncallback=1

    { "places": {
      "place": [
        { "place_id": "bLdkRPtVUb84EQ", "woeid": "43513", "latitude": 51.395, "longitude": -0.010, "place_url": "\/United+Kingdom\/England\/London\/Park+Langley", "place_type": "neighbourhood", "place_type_id": 22, "timezone": "Europe\/London", "name": "Park Langley, Beckenham, England, GB, United Kingdom", "woe_name": "Park Langley" }
      ], "latitude": 51.4, "longitude": 0, "accuracy": 16, "total": 1 }, "stat": "ok" }

      {"stat":"fail","code":100,"message":"Invalid API Key (Key not found)"}

    */
