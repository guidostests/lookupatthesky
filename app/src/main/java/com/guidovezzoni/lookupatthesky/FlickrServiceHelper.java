package com.guidovezzoni.lookupatthesky;

import android.app.Activity;

import retrofit.Callback;
import retrofit.RetrofitError;
import retrofit.client.Response;

/**
 * Created by Guido on 11/06/2015.
 *
 * Handles callbacks from retrofit and some basic check on the returned data
 */
class FlickrServiceHelper implements Callback<FlickrAPIClient.Root> {
    IFlickrServiceCaller mCaller;
    Activity mActivity;

    public FlickrServiceHelper(IFlickrServiceCaller mCaller, Activity mActivity) {
        this.mCaller = mCaller;
        this.mActivity = mActivity;
    }

    @Override
    public void success(FlickrAPIClient.Root root, Response response) {
        // checks that all classes are instantiated before accessing the data
        if (root != null) {
            if (root.stat.equals("ok")) {
                if (root.places != null && root.places.place != null) {
                    if (root.places.place.size() > 0) {
                        mCaller.onFlickrServiceSuccess(root.places.place.get(0).woeid);
                    } else {
                        mCaller.onFlickrServiceError(mActivity.getString(R.string.flicker_error_no_woeid));
                    }
                } else {
                    mCaller.onFlickrServiceError(mActivity.getString(R.string.flicker_error_data_null));
                }
            } else {
                mCaller.onFlickrServiceError(mActivity.getString(R.string.flicker_error_request_failed) + " - " +
                        root.message);
            }
        } else {
            mCaller.onFlickrServiceError(mActivity.getString(R.string.flicker_error_data_null));
        }

    }

    @Override
    public void failure(RetrofitError error) {
        mCaller.onRetrofitError(error);
    }

    public interface IFlickrServiceCaller {

        public void onFlickrServiceSuccess(int woeid);

        public void onFlickrServiceError(String error);

        public void onRetrofitError(RetrofitError error);

    }
}
