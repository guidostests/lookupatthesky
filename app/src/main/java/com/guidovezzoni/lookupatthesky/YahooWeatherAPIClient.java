package com.guidovezzoni.lookupatthesky;



import retrofit.Callback;
import retrofit.RestAdapter;
import retrofit.http.GET;
import retrofit.http.Query;

/**
 * Created by Guido on 10/06/2015.
 */
public class YahooWeatherAPIClient {
    public static final String URL = "https://query.yahooapis.com";

    // constants used for querying the API
    public static final String WEATHER_CONDITION_BY_WOEID_STATEMENT = "select item.condition from weather.forecast where woeid =";
    public static final String FORMAT = "json";
    public static final String ENV = "store%3A%2F%2Fdatatables.org%2Falltableswithkeys";

    public static String getWeatherConditionByWoeidSQL(int woeid) {
        return WEATHER_CONDITION_BY_WOEID_STATEMENT + Integer.toString(woeid);
    }

    //json data class
    static class Root {
        Query query;
    }

    //json data class
    static class Query {
        // count
        // created
        // lang
        Results results;
    }

    //json data class
    static class Results {
        Channel channel;
    }

    //json data class
    static class Channel {
        Item item;
    }

    //json data class
    static class Item {
        Condition condition;
    }

    //json data class
    static class Condition {
        // code
        // date
        int temp;
        String text;
    }


    // Handles the retrofix call
    interface YahooWeatherServiceInterface {

        @GET("/v1/public/yql")
        void execYQLQuery(
                          @retrofit.http.Query("q") String sql,  //q=select%20item.condition%20from%20weather.forecast%20where%20woeid%20%3D%202487889
                          @retrofit.http.Query("format") String format, //format=json
                          @retrofit.http.Query("env") String env, // env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys
                          Callback<Root> callback);
    }

    // Returns a service instance, that will query the API when needed
    public static YahooWeatherServiceInterface getService() {
        RestAdapter restAdapter = new RestAdapter.Builder()
                .setEndpoint(URL)
                .build();

        return restAdapter.create(YahooWeatherServiceInterface.class);
    }

}

    /* example:
    https://query.yahooapis.com/v1/public/yql?q=select%20item.condition%20from%20weather.forecast%20where%20woeid%20%3D%202487889&format=json&env=store%3A%2F%2Fdatatables.org%2Falltableswithkeys

{
 "query": {
  "count": 1,
  "created": "2015-06-10T17:25:19Z",
  "lang": "en-US",
  "results": {
   "channel": {
    "item": {
     "condition": {
      "code": "28",
      "date": "Wed, 10 Jun 2015 9:49 am PDT",
      "temp": "67",
      "text": "Mostly Cloudy"
     }
    }
   }
  }
 }
}
    */
